﻿using System;
using System.IO;
namespace _16_09_2021_zanyatie

{
    class Program
    {
        static void Main(string[] args)
        {
            string path = @"D:\UD01\buzui\csharp\16-09-2021 zanyatie\16-09-2021 zanyatie\word_rus.txt";

            Word word = new Word(GetWord(path));
            
            int error_count = 10;

            while (error_count > 0 && !word.IsWordResolve())
            {
                Console.WriteLine(word.GetCurrentState());
                Console.WriteLine("Введите букву");
                char letter = Console.ReadLine()[0];
                Console.Clear();
                if (word.CheckLetter(letter) == false)
                {
                    error_count--;
                    Console.WriteLine("Нет такой буквы. У Вас осталось " + error_count + " жизней.");
                }
                else
                {
                    Console.WriteLine("Вы угадали букву.");
                }

            }
            Console.Clear();
            if (error_count <= 0)
            {
                Console.WriteLine("Вы проиграли.");
            }
            else
            {
                Console.WriteLine("Вы победили.");
            }
            Console.WriteLine("Слово - " + word.GetWord());
        }
        static string GetWord(string path)
        {
            string[] words = File.ReadAllLines(path);
            Random r = new Random();

            int i = Convert.ToInt32(r.Next(0, words.Length));
            string word = words[i];

            //string word = words[r.Next(0, words.Length)]; - 2й способ

            return word;
        }
    }


}
