﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _16_09_2021_zanyatie
{
    public class Word
    {
        private string word;
        private char[] charWord;
        public Word(string str)
        {
            word = str;
            charWord = new char[word.Length];
            for (int i = 0; i < charWord.Length; i++)
            {
                charWord[i] = '*';
            }
        }
       /// <summary>
       /// проверяет есть ли в слове введенная буква
       /// </summary>
       /// <param name="ch">Буква, которую мы ищем</param>
       /// <returns>true если есть и false если буквы нет</returns>
        public bool CheckLetter(char ch)
        {
            bool isLetterExsist = false;
            for (int i = 0; i < word.Length; i++)
            {
                if (ch == word[i])
                {
                    charWord[i] = ch;
                    isLetterExsist = true;
                }
            }
            return isLetterExsist;
        }
        public string GetCurrentState()
        {
            return new string(charWord);
        }
        public bool IsWordResolve()
        {
            for (int i = 0; i < charWord.Length; i++)
            {
                if (charWord[i] == '*')
                {
                    return false;
                }
            }
            return true;
        }
        /// <summary>
        /// возвращает слово
        /// </summary>
        /// <returns></returns>
        public string GetWord()
        {
            return word;
        }
    }
}
