﻿using System;

namespace _21_09_21_zan
{
    class Program
    {
        static void Main(string[] args)
        {
            Person person = new Person();
            Person person2 = new Person("ivan", 20);
            person.WriteInfo();
            person.SetName("Anna");
            person.SetAge(10);
            person.WriteInfo();
            person2.WriteInfo();

            PersonStruct person_struct; //к структурам можем так обращаться, так их вызывать. не надо через new
        }

    }
}

public class Person
{
    private string name = string.Empty;
    private int age = 0;
    public Person() //konstructor po umolchaniy
    {
        name = "name";
        age = 1;
    }
    public Person(string name, int age) //конструктор полный
    {
        this.name = name; //ссылка класса на самого себя. .this указывает на глобальную переменную класса. Вторая переменная не требует уточнений.
        this.age = age;
    }
    public void SetAge(int a)
    {
        if (a > 0)
        {
            age = a;
        }
    }
    public void SetName(string n)
    {
        name = n;
    }
    public void WriteInfo()
    {
        Console.WriteLine(name + ", " + age);
    }
}
struct PersonStruct
{
    public int age;
    public string name;

}
