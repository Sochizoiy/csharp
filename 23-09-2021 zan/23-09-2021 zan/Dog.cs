﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _23_09_2021_zan
{
    public class Dog : Animal
        
    {
        protected int weight;
        public Dog() // : base() здесь писать не нужно, это происходит по умолчанию
        {
            weight = 1;
           
        }
        public Dog(string name, int weight) : base(name) //конструктор отдает name базовому классу, а себе берет weight
        {
            this.weight = weight;
        }
        public override void Sound() //перезаписываем метод
        {
            Console.WriteLine("Гав гав");
        }
    }
}
