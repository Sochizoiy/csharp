﻿using System;

namespace _23_09_2021_zan
{
    class Program
    {
        static void Main(string[] args)
        {
            Animal animal = new Animal();
            animal.Sound();

            Dog dog = new Dog();
            dog.Sound();
            dog.SetName("good boy");

            Cat cat = new Cat();
            cat.Sound();

            Animal animal_1 = dog; // можем обратиться к собаке как к животному. веса не будет (любых данных которых нет в энимал), но все перегрузки и данные что есть в энимал будут собачьи
            animal_1.Sound(); 
            animal_1.WriteName();

        }
    }
}