﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _23_09_2021_zan
{
    public class Animal
    {
        protected string name;

        public Animal()
        {
            name = "name";
        }

        public Animal(string name)
        {
            this.name = name;
        }

        public virtual void Sound() //можем перезаписывать метод в наследниках
        {
            Console.WriteLine("Звук");
        }

        public void SetName(string newname)
        {
            name = newname;
        }

        public void WriteName()
        {
            Console.WriteLine(name);
        }
    }

}
