﻿using System;

namespace Dz14_09_21
{
    class Program
    {
        static void Main(string[] args)
        {
            //с консоли вводятся слова через пробел, программа должна вывести эти слова в алфавитном порядке
            Console.WriteLine("напишите слова через пробел");
            string str = Console.ReadLine();
            string[] newstrings = str.Split(' ');
            for (int i = 0; i < newstrings.Length; i++)
            {
                char temp = newstrings[i][0];
                for (int j = 0; j < newstrings.Length; j++)
                {
                    char temp2 = newstrings[j][0];
                    if (temp < temp2)
                    {
                        string tempstr;
                        tempstr = newstrings[i];
                        newstrings[i] = newstrings[j];
                        newstrings[j] = tempstr;
                    }
                }
            }

            for (int i = 0; i < newstrings.Length; i++)
            {
                Console.Write(newstrings[i] + ' ');
            }
    }
    }
}

