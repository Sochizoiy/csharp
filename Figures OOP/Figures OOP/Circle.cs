﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Figures_OOP
{
   public  class Circle : Figures
    {
        private float radius;

        public Circle()
        {
            radius = 1;
        }
        public Circle (float radius)
        {
            if (radius > 0) 
            {
                this.radius = radius;
            }
         }
        public override void Ploschad()
        {
            s = radius * radius * 3.14f;
            Console.WriteLine(s);
        }
        public override void Perimetr()
        {
            p = 6.28f * radius;
            Console.WriteLine(p);
        }
    }
}
