﻿using System;

namespace Figures_OOP
{
    class Program
    {
        static void Main(string[] args)
        {
            Circle cirle = new Circle(3);
            cirle.Perimetr();
            cirle.Ploschad();
            Square square = new Square();
            square.ChangeDlina(10);
            square.ChangeShir(4);
            square.Ploschad();
            square.Perimetr();

        }
    }
}
