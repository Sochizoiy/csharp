﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Figures_OOP
{
    public class Square : Figures
    {
        protected float dlina;
        protected float shir;
        public Square()
        {
            dlina = 1;
            shir = 1;
        }
        public Square(int dlina, int shir)
        {
            this.dlina = dlina;
            this.shir = shir;
        }
        public override void Ploschad()
        {
            s = dlina * shir;
            Console.WriteLine(s);
        }
       
        public override void Perimetr()
        {
            p = dlina * 2 + shir * 2;
            Console.WriteLine(p);
        }
        public void ChangeDlina(int dlina)
        {
            if (dlina > 0)
            {
                this.dlina = dlina;
            }
        }
        /// <summary>
        /// меняем ширину
        /// </summary>
        /// <param name="shir">ширина</param>
        public void ChangeShir(int shir)
        {
            if (shir > 0)
            {
                this.shir = shir;
            }
        }
    }
}
