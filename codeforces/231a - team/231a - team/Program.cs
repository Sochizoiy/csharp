﻿using System;

namespace _231a___team
{
    class Program
    {
        static void Main(string[] args)
        {
            int zadach = Convert.ToInt32(Console.ReadLine());
            int[,] reshenie = new int[zadach, 3];
            int countz = 0;
            for (int i = 0; i<zadach; i++)
            {
                string str = Console.ReadLine();
                string[] newstrings = str.Split(' ');
                int petya = int.Parse(newstrings[0]);
                int vasya = int.Parse(newstrings[1]);
                int tonya = int.Parse(newstrings[2]);
                reshenie[i, 0] = petya;
                reshenie[i, 1] = vasya;
                reshenie[i, 2] = tonya;
            }
            for (int i = 0; i < zadach; i++)
            {
               if ( reshenie[i, 0] + reshenie[i, 1] + reshenie[i, 2] > 1)
                { countz++; }
            }
            Console.WriteLine(countz);
        }
    }
}
