﻿using System;

namespace _263A_nice_matrix
{
    class Program
    {
        static void Main(string[] args)
        {
            int[,] reshenie = new int[5, 5];
            int nomervert = 0;
            int nomerhor = 0;
            for (int i = 0; i < 5; i++)
            {
                string str = Console.ReadLine();
                string[] newstrings = str.Split(' ');
                for (int k = 0; k < 5; k++)
                {
                    reshenie[i, k] = Convert.ToInt32(newstrings[k]);
                   if (reshenie[i, k] != 0)
                    {
                        nomervert = i+1;
                        nomerhor = k+1;
                    }
                }
            }
            int shagov = 0;
            if (nomerhor < 4)
            {
                shagov += 3 - nomerhor;
            }    
            else
            {
                shagov += -3 +nomerhor;
            }
            if (nomervert < 4)
            {
                shagov += 3 - nomervert;
            }
            else
            {
                shagov += -3 + nomervert;
            }
            Console.WriteLine(shagov);
        }
    }
}
