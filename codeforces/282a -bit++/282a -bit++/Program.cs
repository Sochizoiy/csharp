﻿using System;

namespace _282a__bit__
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = Convert.ToInt32(Console.ReadLine());
            int itog = 0;
            char[,] simv = new char[n, 3];
            string[] kommandy = new string[n];
            for (int i = 0; i < n; i++)
            {
                kommandy[i] = Console.ReadLine();
                simv[i, 0] = kommandy[i][0];
                simv[i, 1] = kommandy[i][1];
                simv[i, 2] = kommandy[i][2];
                if (simv[i, 0] == '+' || simv[i, 1] == '+' || simv[i, 2] == '+')
                {
                    {
                        itog++;
                    }
                }
                else itog--;
            }
            Console.WriteLine(itog);
        }
    }
}
