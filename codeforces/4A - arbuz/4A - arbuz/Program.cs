﻿using System;

namespace _4A___arbuz
{
    class Program
    {
        static void Main(string[] args)
        {
            float i = Convert.ToInt32(Console.ReadLine());
            if (i % 2 == 0 && i >= 4)
            {
                Console.WriteLine("YES");
            }
            else { Console.WriteLine("NO"); }
        }
    }
}