﻿using System;

namespace _50a___domino
{
    class Program
    {
        static void Main(string[] args)
        {
            string str = Console.ReadLine();
            string[] newstrings = str.Split(' ');
            int dlina = int.Parse(newstrings[0]);
            int shir = int.Parse(newstrings[1]);
            int itog;
            if (dlina % 2 != 0 && shir %2 !=0)
            {
                itog = (dlina-1)*shir/2;
                if (dlina > shir)
                {
                    itog += dlina / 2;
                }
                else { itog += shir / 2; }
            }
            else
            {
                itog = dlina * shir / 2;
            }
            Console.WriteLine(itog);
        }
    }
}
