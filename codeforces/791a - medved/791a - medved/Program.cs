﻿using System;

namespace _791a___medved
{
    class Program
    {
        static void Main(string[] args)
        {
            string str = Console.ReadLine();
            string[] newstrings = str.Split(' ');
            int a = int.Parse(newstrings[0]);
            int b = int.Parse(newstrings[1]);
            int years = 0;
            while (a <= b)
            {
                a = a * 3;
                b = b * 2;
                years++;
            }
            Console.WriteLine(years);
        }
    }
}
