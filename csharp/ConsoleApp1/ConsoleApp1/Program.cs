﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] array = new[] { 1, 5, 10, -5, 2, 0 };  //Создали исходный массив
            int summ = 0; //Ввели переменную для суммы чисел массива 
            for (int i = 0; i < array.Length; i++)
            {
                summ = summ + array[i]; 
            } //Нашли сумму чисел исходного массива 
            Console.WriteLine("Сумма чисел в исходном массиве: " + summ); //Вывели сумму

            for (int i = 0; i < array.Length; i++)
            {
                array[i] = array[i] + 5; // Увеличили кажый элемент исходного массива на 5
            }
            Console.WriteLine("Увеличили значение массива на 5: ");
            for (int i = 0; i < array.Length; i++)
            {
                Console.Write(array[i] + " "); // Вывели в строку с пробелом
            }
            float[] array2 = new float[array.Length]; //Создали второй массив той же длины что и первый
            Console.WriteLine("\nНовый массив, значения в 2 раза меньше чем в исходном ");
            for (int i = 0; i < array2.Length; i++)
            {
                array2[i] = array[i] / 2f; //наполняем новый массив теми же значениями, но в 2 раза меньше
                Console.Write(array2[i] + " "); //выводим их в строку через пробел
            }

        }
    }
}
