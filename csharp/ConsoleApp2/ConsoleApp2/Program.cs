﻿using System;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            string str = Console.ReadLine();
            char a = str[0];
            for (int n = 0; n < str.Length; n += 1)
            {
                if (str[n] > a)
                {
                    a = str[n];
                }
            }
            Console.WriteLine(a);
        }
    }
}
