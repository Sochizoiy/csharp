﻿
using System;

namespace ConsoleApp3
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                int vvod = 0;
                Console.WriteLine("\n\n Список заданий:");
                Console.WriteLine("1. Программа вычисления суммы и произведения двух чисел");
                Console.WriteLine("2. Программа вычисления площади прямоугольника");
                Console.WriteLine("3. Программа вычисления площади круга по радиусу");
                Console.WriteLine("4. Программа вычисления среднего арифметического трех чисел");
                Console.WriteLine("5. Дано число, если оно больше 10, то уменьшить его на 10, если меньше то увеличить на 10");
                Console.WriteLine("6. Дано два числа. Если их сумма больше 100, вывести yes, если меньше no");
                Console.WriteLine("7. Дано число. Если оно четное - вывести yes, если нечетное вывести no");
                Console.WriteLine("8. Дан массив чисел. Найти среднее арифметическое его элементов");
                Console.WriteLine("9. Создать массив, элементы которого равны квадрату своего индекса");
                Console.WriteLine("10. Создать и заполнить массив четными числами начиная с 2");
                Console.WriteLine("11. Дан массив чисел, подсчитать количество четных элементов в массиве");
                Console.WriteLine("12. Дан массив чисел. Отсортировать массив по возрастанию");
                Console.WriteLine("13. Дан массив чисел. Найти все нечетные элементы и вывести их по убыванию");
                Console.WriteLine("14. Дан массив чисел. Определить, есть ли в массиве повторяющиеся элементы");
                Console.WriteLine("15. Дана строка. Определить количество повторений каждой буквы в этом слове");
                Console.WriteLine();
                string str = Console.ReadLine();
                Console.WriteLine();
                try { vvod = Convert.ToInt32(str); }
                catch { Console.WriteLine("Вводите цифры плз"); }

                switch (vvod)
                {
                    case 1:
                        Zadanie1();
                        break;
                    case 2:
                        Zadanie2();
                        break;
                    case 3:
                        Zadanie3();
                        break;
                    case 4:
                        Zadanie4();
                        break;
                    case 5:
                        Zadanie5();
                        break;
                    case 6:
                        Zadanie6();
                        break;
                    case 7:
                        Zadanie7();
                        break;
                    case 8:
                        Zadanie8();
                        break;
                    case 9:
                        Zadanie9();
                        break;
                    case 10:
                        Zadanie10();
                        break;
                    case 11:
                        Zadanie11();
                        break;
                    case 12:
                        Zadanie12();
                        break;
                    case 13:
                        Zadanie13();
                        break;
                    case 14:
                        Zadanie14();
                        break;
                    case 15:
                        Zadanie15();
                        break;
                    default:
                        Console.WriteLine("Неправильно указан номер задания");
                        break;
                }
                 
                ConsoleKeyInfo key;
                Console.WriteLine("Нажмите Esc для выхода в меню выбора программ.");
                key = Console.ReadKey();
                while ( key.Key!= ConsoleKey.Escape) 
                {
                    Console.WriteLine("\nНажмите Esc для выхода в меню выбора программ.");
                    key = Console.ReadKey();
                }    
                


                static void Zadanie1()
                {
                    Console.WriteLine("Введите первое число:");
                    string s = Console.ReadLine();
                    int i = Convert.ToInt32(s);
                    Console.WriteLine("Введите второе число:");
                    string s2 = Console.ReadLine();
                    int i2 = Convert.ToInt32(s2);
                    Console.WriteLine("Сумма чисел: " + (i + i2));
                    Console.WriteLine("Произведение чисел: " + i * i2);
                }
                static void Zadanie2()
                {
                    Console.WriteLine("Введите длину");
                    string s = Console.ReadLine();
                    int i = Convert.ToInt32(s);
                    Console.WriteLine("Введите ширину");
                    string s2 = Console.ReadLine();
                    int i2 = Convert.ToInt32(s2);
                    Console.WriteLine("Площадь прямоугольника: " + i * i2);
                }
                static void Zadanie3()
                {
                    Console.WriteLine("Введите радиус:");
                    string s = Console.ReadLine();
                    int i = Convert.ToInt32(s);
                    Console.WriteLine("Площадь круга: " + i * i * 3.14f);
                }
                static void Zadanie4()
                {
                    Console.WriteLine("Введите 1 число");
                    string s = Console.ReadLine();
                    int i = Convert.ToInt32(s);
                    Console.WriteLine("Введите 2 число ");
                    string s2 = Console.ReadLine();
                    int i2 = Convert.ToInt32(s2);
                    Console.WriteLine("Введите 3 число ");
                    string s3 = Console.ReadLine();
                    int i3 = Convert.ToInt32(s3);
                    float sredn = (i3 + i2 + i) / 3f;
                    Console.WriteLine("Среднее арифметическое: " + sredn);
                }
                static void Zadanie5()
                {
                    Console.WriteLine("Введите число: ");
                    string s = Console.ReadLine();
                    int i = Convert.ToInt32(s);
                    if (i > 10)
                    {
                        i = i - 10;
                    }
                    else { i = i + 10; }
                    Console.WriteLine("Число после изменения: " + i);
                }
                static void Zadanie6()
                {
                    Console.WriteLine("Введите 1 число");
                    string s = Console.ReadLine();
                    int i = Convert.ToInt32(s);
                    Console.WriteLine("Введите 2 число");
                    string s2 = Console.ReadLine();
                    int i2 = Convert.ToInt32(s2);
                    int summ = i + i2;
                    if (summ > 100)
                    {
                        Console.WriteLine("yes");
                    }
                    else
                    {
                        Console.WriteLine("no");
                    }
                }
                static void Zadanie7()
                {
                    Console.WriteLine("Введите число:");
                    string s = Console.ReadLine();
                    int i = Convert.ToInt32(s);
                    int a = i % 2; //загуглил + законспектировал как проверить остаток
                    if (a == 0)
                    {
                        Console.WriteLine("yes");
                    }
                    else
                    {
                        Console.WriteLine("no");
                    }
                }
                static void Zadanie8()
                {
                    int[] array = new[] { 1, 13, 25, 16, -21, 0 };
                    int summ = 0;
                    for (int i = 0; i < array.Length; i++)
                    {
                        summ = summ + array[i];
                    }
                    float sredn = summ * 1f / array.Length; //Домножаем на 1f сумму, так как конвертируем во float. Не работало по-другому.
                    Console.WriteLine("Среднее арифметическое: " + sredn);
                }
                static void Zadanie9()
                {
                    Console.WriteLine("Введите количество элементов массива.");
                    string s = Console.ReadLine();
                    int k = Convert.ToInt32(s);
                    int[] array = new int[k];
                    for (int a = 0; a < k; a++)
                    {
                        array[a] = a * a;
                        Console.Write(array[a] + "; ");
                    }
                }
                static void Zadanie10()
                {
                    Console.WriteLine("Введите количество элементов массива.");
                    string s = Console.ReadLine();
                    int k = Convert.ToInt32(s);
                    int[] array = new int[k];
                    for (int a = 0; a < k; a++)
                    {
                        array[a] = 2 + a * 2; //т.к. первый индекс - 0, прибавим 2
                        Console.Write(array[a] + "; ");
                    }
                }
                static void Zadanie11()
                {
                    int[] array = new[] { 1, 13, 25, 16, -21, 0 };
                    int z = 0;
                    for (int i = 0; i < array.Length; i++)
                    {
                        int a = array[i] % 2;
                        if (a == 0)
                        {
                            z++;
                        }
                    }
                    Console.WriteLine("Четных чисел: " + z);
                }
                static void Zadanie12()
                {
                    int[] array = new[] { 1, 13, 25, 16, -21, 0 };
                    int temp; //переменная для хранения изменяемого элемента
                    for (int i = 0; i < array.Length; i++)
                    {
                        for (int f = 0; f < array.Length; f++) //в этом цикле мы проверяем каждый элемент
                        {
                            if (array[i] < array[f])  // с каждым элементом
                            {
                                temp = array[i]; //и меняем их местами, если нужно.
                                array[i] = array[f];
                                array[f] = temp;
                            }
                        }
                    }
                    for (int a = 0; a < array.Length; a++) //затем выводим.
                    {
                        Console.Write(array[a] + "; ");
                    }
                }
                static void Zadanie13()
                {
                    int[] array = new[] { 1, 13, 25, 16, -21, 0 };
                    int temp;
                    for (int i = 0; i < array.Length; i++)
                    {
                        for (int f = 0; f < array.Length; f++)
                        {
                            if (array[i] > array[f])
                            {
                                temp = array[i];
                                array[i] = array[f];
                                array[f] = temp;
                            }
                        }
                    }
                    for (int a = 0; a < array.Length; a++)
                    {
                        if (array[a] % 2 != 0) //тоже что и в прошлом задании, но выводим при условии отсутствия остатка при делении на 2.
                        {
                            Console.Write(array[a] + "; ");
                        }
                    }
                }
                static void Zadanie14()
                {
                    int[] array = new[] { 1, 13, 25, 16, -21, 0, -24 };
                    int temp = 0; //Вводим переменную, которую увеличим, если найдем хоть одно повторение
                    for (int i = 0; i < array.Length; i++)
                    {
                        for (int f = i + 1; f < array.Length; f++)
                        {
                            if (array[i] == array[f])
                            {
                                temp += 1; //увеличиваем при совпадении
                            }
                        }
                    }
                    if (temp == 0) //Добавляем условие на вывод текста.
                    {
                        Console.WriteLine("Массив без повторений.");
                    }
                    else
                    {
                        Console.WriteLine("Есть повторения в массиве.");
                    }
                }
                static void Zadanie15()
                {
                    string str = Console.ReadLine();
                    int count = 1; //Вводим переменную для счета букв
                    char[] array = new char[str.Length]; //Создаем массив
                    Console.WriteLine("Количество букв в слове:");
                    for (int i = 0; i < str.Length; i += 1)
                    {
                        if (array[i] != '*') //Мы будем считать все символы, кроме звёздочки
                        {
                            count = 1; //каждый раз при подсчете новой буквы, обнуляем до еденицы подсчет повторок
                            array[i] = str[i]; // наш массив наполняем как строку
                            for (int f = i + 1; f < str.Length; f += 1) // тут сравнивать можно не с каждым элементом, а с теми что идут после.
                            {
                                if (str[i] == str[f])
                                {
                                    count = count + 1; //При совпадении букв увеличиваем счетчик
                                    array[f] = '*'; //если видим, что наша буква уже используется позже, в массив вместо нее подкладываем звездочку, чтобы не прописывать ее второй раз
                                }
                            }
                            Console.WriteLine(array[i] + ": " + count);
                        }
                    }
                }
            }
        }
    }
}

