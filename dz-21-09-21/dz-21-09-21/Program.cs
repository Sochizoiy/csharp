﻿using System;

namespace dz_21_09_21
{
    class Program
    {
        static void Main(string[] args)
        {
            Person defaultperson = new Person();
            Person customperson = new Person("Максим", "Бузуй", "муж", 24);
            defaultperson.WriteInfo();
            customperson.WriteInfo();
            defaultperson.SetSex("жен");
            defaultperson.SetAge(39);
            defaultperson.SetLastName("Тихановская");
            defaultperson.SetName("Светлана");
            defaultperson.WriteInfo();
        }
    }
}
public class Person
{
    private string name = string.Empty;
    private string lastname = string.Empty;
    private string sex = string.Empty;
    private int age = 0;
    public Person() //конструктор по умолчанию
    {
        name = "name";
        lastname = "lastname";
        sex = "sex";
        age = 0;
    }
    public Person(string name,string lastname, string sex, int age) //конструктор полный
    {
        this.name = name;
        //ссылка класса на самого себя. .this указывает на глобальную переменную класса. Вторая переменная не требует уточнений.
        this.lastname = lastname;
        this.sex = sex;
        this.age = age;
    }
    /// <summary>
    /// меняем возраст
    /// </summary>
    /// <param name="a">возраст</param>
    public void SetAge(int a)
    {
        if (a > 0)
        {
            age = a;
        }
    }
    /// <summary>
    /// меняем имя
    /// </summary>
    /// <param name="n">новое имя</param>
    public void SetName(string n)
    {
        name = n;
    }
    public void SetLastName(string lastname)
    {
        this.lastname = lastname;
    }
    public void SetSex(string sex)
    {
        this.sex = sex;
    }
    /// <summary>
    /// выводим информацию о человеке на экран
    /// </summary>
    public void WriteInfo()
    {
        Console.WriteLine(name + " " + lastname + ", " + sex + ", " + age);
    }
}
