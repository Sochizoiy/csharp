﻿using System;

namespace z_10
{
    class Program
    {
        static void Main(string[] args)
        {
            //Создать и заполнить массив четными числами начиная с 2.
            Console.WriteLine("Введите количество элементов массива.");
            string s = Console.ReadLine();
            int k = Convert.ToInt32(s);
            int[] array = new int[k];
            for (int a = 0; a < k; a++)
            {
                array[a] = 2 + a * 2; //т.к. первый индекс - 0, прибавим 2
                Console.Write(array[a] + "; ");
            }
        }
    }
}
