﻿using System;

namespace z_11
{
    class Program
    {
        static void Main(string[] args)
        {
            //Дан массив чисел, подсчитать количество четных элементов в массиве.
            int[] array = new[] { 1, 13, 25, 16, -21, 0 };
            int z = 0;
            for (int i = 0; i < array.Length; i++)
            {
                int a = array[i] % 2;
                if (a == 0)
                {
                    z++;
                }
            }
            Console.WriteLine("Четных чисел: " + z);
        }
    }
}
