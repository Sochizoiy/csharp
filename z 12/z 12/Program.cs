﻿using System;

namespace z_12
{
    class Program
    {
        static void Main(string[] args)
        {
            //Дан массив чисел. Отсортировать массив по возрастанию. (Реализовать сортировку пузырьком).
            int[] array = new[] { 1, 13, 25, 16, -21, 0 };
            int temp; //переменная для хранения изменяемого элемента
            for (int i =0; i<array.Length; i++)
            {
                for (int f=0; f<array.Length; f++) //в этом цикле мы проверяем каждый элемент
                {
                    if (array[i] < array[f])  // с каждым элементом
                    {
                        temp = array[i]; //и меняем их местами, если нужно.
                        array[i] = array[f];
                        array[f] = temp;
                    }
                }
            }
            for (int a=0; a<array.Length; a++) //затем выводим.
            {
                Console.Write(array[a] + "; ");
            }
        }
    }
}
