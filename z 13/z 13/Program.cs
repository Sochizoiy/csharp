﻿using System;

namespace z_13
{
    class Program
    {
        static void Main(string[] args)
        {
            //Дан массив чисел. Найти все нечетные элементы и вывести их по убыванию.
            int[] array = new[] { 1, 13, 25, 16, -21, 0 };
            int temp;
            for (int i = 0; i < array.Length; i++)
            {
                for (int f = 0; f < array.Length; f++)
                {
                    if (array[i] > array[f])
                    {
                        temp = array[i];
                        array[i] = array[f];
                        array[f] = temp;
                    }
                }
            }
            for (int a = 0; a < array.Length; a++)
            {
                if (array[a] % 2 != 0) //тоже что и в прошлом задании, но выводим при условии отсутствия остатка при делении на 2.
                {
                    Console.Write(array[a] + "; ");
                }
            }
        }
    }
}
