﻿using System;

namespace z_14
{
    class Program
    {
        static void Main(string[] args)
        {
            //Дан массив чисел. Определить, есть ли в массиве повторяющиеся элементы.
            int[] array = new[] { 1, 13, 25, 16, -21, 0 , -24};
            int temp= 0; //Вводим переменную, которую увеличим, если найдем хоть одно повторение
            for (int i = 0; i < array.Length; i++)
            {
                for (int f = i + 1; f < array.Length; f++)
                {
                    if (array[i]== array[f])
                    {
                        temp += 1; //увеличиваем при совпадении
                    }
                }
            }
            if (temp == 0) //Добавляем условие на вывод текста.
            {
                Console.WriteLine("Массив без повторений.");
            }
            else
            {
                Console.WriteLine("Есть повторения в массиве.");
            }
        }
    }
}
