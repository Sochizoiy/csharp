﻿using System;

namespace z_3
{
    class Program
    {
        static void Main(string[] args)
        {
            //Написать программу вычисления площади круга по радиусу.
            Console.WriteLine("Введите радиус:");
            string s = Console.ReadLine();
            int i = Convert.ToInt32(s);
            Console.WriteLine("Площадь круга: " + i*i*3.14f);
        }
    }
}
