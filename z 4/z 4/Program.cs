﻿using System;

namespace z_4
{
    class Program4
    {
        static void Main(string[] args)
        {
            //Написать программу вычисления среднего арифметического трех чисел.
            Console.WriteLine("Введите 1 число");
            string s = Console.ReadLine();
            int i = Convert.ToInt32(s);
            Console.WriteLine("Введите 2 число ");
            string s2 = Console.ReadLine();
            int i2 = Convert.ToInt32(s2);
            Console.WriteLine("Введите 3 число ");
            string s3 = Console.ReadLine();
            int i3 = Convert.ToInt32(s3);
            float sredn = (i3 + i2 + i) / 3f;
            Console.WriteLine("Среднее арифметическое: " + sredn);
        }
    }
}
