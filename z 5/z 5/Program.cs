﻿using System;

namespace z_5
{
    class Program5
    {
        static void Main(string[] args)
        {
            //Дано число, если оно больше 10, то уменьшить его на 10, если меньше то увеличить на 10.
            Console.WriteLine("Введите число: ");
            string s = Console.ReadLine();
            int i = Convert.ToInt32(s);
            if (i > 10)
            {
                i = i - 10;
            }
            else { i = i + 10; }
            Console.WriteLine("Число после изменения: " + i);
        }
    }
}
