﻿using System;

namespace z_7
{
    class Program
    {
        static void Main(string[] args)
        {
            //Дано число. Если оно четное - вывести "yes", если нечетное вывести "no".
            Console.WriteLine("Введите число:");
            string s = Console.ReadLine();
            int i = Convert.ToInt32(s);
            int a = i % 2; //загуглил + законспектировал как проверить остаток
            if (a == 0)
            {
                Console.WriteLine("yes");
            }
            else
            {
                Console.WriteLine("no");
            }
        }
    }
}
