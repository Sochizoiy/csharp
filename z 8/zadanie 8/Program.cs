﻿using System;

namespace zadanie_8
{
    class Program8
    {
        static void Main(string[] args)
        {
            //Дан массив чисел. Найти среднее арифметическое его элементов.
            int[] array = new[] { 1, 13, 25, 16, -21, 0 };
            int summ = 0;
            for (int i = 0; i< array.Length; i++)
            {
                summ = summ + array[i];
            }
            float sredn = summ * 1f / array.Length; //Домножаем на 1f сумму, так как конвертируем во float. Не работало по-другому.
            Console.WriteLine("Среднее арифметическое: " + sredn);
        }
    }
}
