﻿using System;

namespace zadanie_9
{
    class Program
    {
        static void Main(string[] args)
        {
            //Создать массив, элементы которого равны квадрату своего индекса.
            Console.WriteLine("Введите количество элементов массива.");
            string s = Console.ReadLine();
            int k = Convert.ToInt32(s);
            int[] array = new int[k];
            for (int a = 0; a < k; a++)
            {
                array[a] = a * a;
                Console.Write(array[a]+ "; ");
            }
        }
    }
}
